﻿namespace Songwunsch
{
    partial class frm_songs
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_artist = new System.Windows.Forms.Label();
            this.lbl_songname = new System.Windows.Forms.Label();
            this.lbl_genre = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.txt_band = new System.Windows.Forms.TextBox();
            this.txt_titel = new System.Windows.Forms.TextBox();
            this.cbx_genre = new System.Windows.Forms.ComboBox();
            this.lbl_titel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbl_artist
            // 
            this.lbl_artist.AutoSize = true;
            this.lbl_artist.Location = new System.Drawing.Point(32, 102);
            this.lbl_artist.Name = "lbl_artist";
            this.lbl_artist.Size = new System.Drawing.Size(90, 16);
            this.lbl_artist.TabIndex = 0;
            this.lbl_artist.Text = "Band/Künstler";
            // 
            // lbl_songname
            // 
            this.lbl_songname.AutoSize = true;
            this.lbl_songname.Location = new System.Drawing.Point(32, 160);
            this.lbl_songname.Name = "lbl_songname";
            this.lbl_songname.Size = new System.Drawing.Size(33, 16);
            this.lbl_songname.TabIndex = 1;
            this.lbl_songname.Text = "Titel";
            // 
            // lbl_genre
            // 
            this.lbl_genre.AutoSize = true;
            this.lbl_genre.Location = new System.Drawing.Point(32, 223);
            this.lbl_genre.Name = "lbl_genre";
            this.lbl_genre.Size = new System.Drawing.Size(44, 16);
            this.lbl_genre.TabIndex = 2;
            this.lbl_genre.Text = "Genre";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(35, 308);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(97, 45);
            this.button1.TabIndex = 3;
            this.button1.Text = "Abschicken";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(162, 308);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(95, 45);
            this.button2.TabIndex = 4;
            this.button2.Text = "Beenden";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // txt_band
            // 
            this.txt_band.Location = new System.Drawing.Point(141, 100);
            this.txt_band.MaxLength = 50;
            this.txt_band.Name = "txt_band";
            this.txt_band.Size = new System.Drawing.Size(128, 22);
            this.txt_band.TabIndex = 5;
            // 
            // txt_titel
            // 
            this.txt_titel.Location = new System.Drawing.Point(141, 160);
            this.txt_titel.MaxLength = 50;
            this.txt_titel.Name = "txt_titel";
            this.txt_titel.Size = new System.Drawing.Size(128, 22);
            this.txt_titel.TabIndex = 6;
            // 
            // cbx_genre
            // 
            this.cbx_genre.FormattingEnabled = true;
            this.cbx_genre.Items.AddRange(new object[] {
            "Rock",
            "Pop",
            "Rap",
            "RnB"});
            this.cbx_genre.Location = new System.Drawing.Point(141, 223);
            this.cbx_genre.Name = "cbx_genre";
            this.cbx_genre.Size = new System.Drawing.Size(128, 24);
            this.cbx_genre.TabIndex = 7;
            this.cbx_genre.Text = "Rock";
            // 
            // lbl_titel
            // 
            this.lbl_titel.AutoSize = true;
            this.lbl_titel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_titel.Location = new System.Drawing.Point(69, 22);
            this.lbl_titel.Name = "lbl_titel";
            this.lbl_titel.Size = new System.Drawing.Size(158, 25);
            this.lbl_titel.TabIndex = 8;
            this.lbl_titel.Text = "3D-Songwunsch";
            // 
            // frm_songs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Lavender;
            this.ClientSize = new System.Drawing.Size(297, 381);
            this.Controls.Add(this.lbl_titel);
            this.Controls.Add(this.cbx_genre);
            this.Controls.Add(this.txt_titel);
            this.Controls.Add(this.txt_band);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.lbl_genre);
            this.Controls.Add(this.lbl_songname);
            this.Controls.Add(this.lbl_artist);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_songs";
            this.Text = "Songwunsch";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_artist;
        private System.Windows.Forms.Label lbl_songname;
        private System.Windows.Forms.Label lbl_genre;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox txt_band;
        private System.Windows.Forms.TextBox txt_titel;
        private System.Windows.Forms.ComboBox cbx_genre;
        private System.Windows.Forms.Label lbl_titel;
    }
}

