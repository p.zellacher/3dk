﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Songwunsch
{
    public partial class frm_songs : Form
    {
        public frm_songs()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(txt_band.Text != "" && txt_titel.Text != "")
            {
                cls_song c = new cls_song(txt_band.Text, txt_titel.Text, cbx_genre.Text);

                cls_Dataprovider.InsertData(c);

                txt_band.Text = "";
                txt_titel.Text = "";
            }
            else
            {
                MessageBox.Show("Bitte alle Felder ausfüllen!");
            }
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
