﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Songwunsch
{
    internal class cls_song
    {
        int m_id;
        string m_song;
        string m_band;
        string m_genre;

        public int Id { get => m_id; set => m_id = value; }
        public string Song { get => m_song; set => m_song = value; }
        public string Band { get => m_band; set => m_band = value; }
        public string Genre { get => m_genre; set => m_genre = value; }

        public cls_song( string band, string song, string genre)
        {
            m_band= band;
            m_song = song;
            m_genre = genre;
        }
    }
}
